<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                ارسال خبر جدید
            </header>
            <div class="panel-body">
                <form role="form" action="news_query.php?type=insert" method="POST" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="form-group">
                        <label for="exampleInputEmail1">محل نمایش خبر در سایت :  </label>
                        <input type="radio" name="radio" value="1"> نمایش در قسمت وسط
                        <input type="radio" name="radio" value="2"> نمایش در منوی کناری
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان خبر</label>
                        <input type="input" class="form-control" name="onvan" placeholder="خبر جدید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن خبر</label>
                        <textarea name="editor"></textarea>
                    </div>
                    <script type="text/javascript">
                    CKEDITOR.replace( 'editor' , {
                    filebrowserUploadUrl: "test.php"
                    } );
                    </script>
                    <div class="form-group">
                        <label for="exampleInputFile">ارسال تصویر شاخص برای خبر</label>
                        <input type="file" name="news_pic">
                    </div>
                    <button type="submit" class="btn btn-info">ارسال</button>
                </form>
            </div>
        </section>
    </div>
</div>