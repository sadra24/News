<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Learning Project</title>
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">
		<link href="css/jsgrid.css" rel="stylesheet">
		<link href="css/jsgrid.min.css" rel="stylesheet">
		<link href="css/ui.jqgrid.css" rel="stylesheet">
		<link href="css/ui.jqgrid-bootstrap.css" rel="stylesheet">
		<link href="css/ui.jqgrid-bootstrap-ui.css" rel="stylesheet">
		<link href="css/jquery-ui.css" rel="stylesheet">
		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- <script src="js/ie10-viewport-bug-workaround.js"></script> -->
		<script src="js/jquery.jqGrid.min.js"></script>
		<script src="js/grid.locale-en.js"></script>
		<!-- <script src="js/jquery.jqGrid.src.js"></script> -->
	</head>
	<body>
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbar">
			</div>
		</div>